# Contributor: Keith Maxwell <keith.maxwell@gmail.com>
# Maintainer: Roberto Oliveira <robertoguimaraes8@gmail.com>
pkgname=py3-google-api-python-client
_pkgname=google-api-python-client
pkgver=2.78.0
pkgrel=0
pkgdesc="Google API Client Library for Python"
url="https://github.com/googleapis/google-api-python-client"
arch="noarch !ppc64le"  # limited by py3-grpcio
license="Apache-2.0"
depends="
	py3-google-api-core
	py3-google-auth
	py3-google-auth-httplib2
	py3-httplib2
	py3-oauth2client
	py3-uritemplate
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="
	py3-mock
	py3-openssl
	py3-parameterized
	py3-pytest
	"
source="https://files.pythonhosted.org/packages/source/g/google-api-python-client/google-api-python-client-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-google-api-python-client" # Backwards compatibility
provides="py-google-api-python-client=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
7a52b6beed7d496a888293ff99a95a7ef634f1753b66a8c215dcf9483902e626febf82d77bc880fe38d0a85cafa20c96e0eb10736da1ae8e317b97f5e8b55c1e  google-api-python-client-2.78.0.tar.gz
"
