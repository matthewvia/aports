# Contributor: Dermot Bradley <dermot_bradley@yahoo.com>
# Maintainer: Laurent Bercot <ska-devel@skarnet.org>
pkgname=s6-overlay
pkgver=3.1.4.0
pkgrel=0
pkgdesc="s6 overlay for containers"
url="https://github.com/just-containers/s6-overlay/"
arch="noarch"
license="ISC"
depends="execline s6 s6-rc s6-linux-init s6-portable-utils s6-linux-utils s6-dns s6-networking s6-overlay-helpers"
subpackages="$pkgname-doc $pkgname-syslogd"
options="!check"
source="$pkgname-$pkgver.tar.gz::https://github.com/just-containers/s6-overlay/archive/refs/tags/v$pkgver.tar.gz"

build() {
	make rootfs-overlay-noarch symlinks-overlay-noarch syslogd-overlay-noarch \
		"VERSION=$pkgver" \
		SHEBANGDIR=/usr/bin
}

package() {
	mkdir -p "$pkgdir"
	cp -a "$srcdir/$pkgname-$pkgver/output/rootfs-overlay-noarch"/* \
		"$srcdir/$pkgname-$pkgver/output/symlinks-overlay-noarch"/* \
		"$pkgdir/"
}

doc() {
	default_doc
	mkdir -p "$subpkgdir/usr/share/doc/$pkgname"
	cp "$srcdir/$pkgname-$pkgver/AUTHORS.md" \
		"$srcdir/$pkgname-$pkgver/CHANGELOG.md" \
		"$srcdir/$pkgname-$pkgver/CONTRIBUTING" \
		"$srcdir/$pkgname-$pkgver/COPYING" \
		"$srcdir/$pkgname-$pkgver/DCO" \
		"$srcdir/$pkgname-$pkgver/MOVING-TO-V3.md" \
		"$srcdir/$pkgname-$pkgver/README.md" \
		"$subpkgdir/usr/share/doc/$pkgname/"
}

syslogd() {
	mkdir -p "$subpkgdir"
	cp -a "$srcdir/$pkgname-$pkgver/output/syslogd-overlay-noarch"/* "$subpkgdir/"
}

sha512sums="
0de8384e47b0322a91860b1a27250e4efc6662a3b74a401182ae512ea9d0caf04b7c323e16f3574cac644d4947eabdbf04a3488d9daf172ff400595022af2848  s6-overlay-3.1.4.0.tar.gz
"
