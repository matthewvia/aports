# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kscreen
pkgver=5.27.0
pkgrel=0
pkgdesc="KDE's screen management software"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later AND LGPL-2.1-or-later"
depends="hicolor-icon-theme"
makedepends="
	extra-cmake-modules
	kcmutils-dev
	kconfig-dev
	kconfigwidgets-dev
	kdbusaddons-dev
	kdeclarative-dev
	kglobalaccel-dev
	ki18n-dev
	kiconthemes-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	layer-shell-qt-dev
	libkscreen-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtsensors-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
source="https://download.kde.org/$_rel/plasma/$pkgver/kscreen-$pkgver.tar.xz"
subpackages="$pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kscreen-kded-configtest is broken
	# kscreen-kded-osdtest hangs
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kscreen-kded-(config|osd)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9264b5d18186e17d4d5b462d7a91b8d20e779fdda02ef0295d261517b91ba04ac01566235b90189867b4d6217e082bbd9d99c2b12af83e8fa9581de8704e0c61  kscreen-5.27.0.tar.xz
"
